package uz.pdp.lesson37pdp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import uz.pdp.lesson37pdp1.views.SwitchButton;

public class MainActivity extends AppCompatActivity implements SwitchButton.OnSwitchListener{
    private SwitchButton switchButton;
    private SwitchButton switchButton1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        switchButton = findViewById(R.id.switch_button);
        switchButton1 = findViewById(R.id.switch_button1);
        switchButton.setOnSwitchListener(this);
//        switchButton1.setOnSwitchListener(this);
    }

    @Override
    public void onSwitch(SwitchButton switchButton, boolean mSwitch) {
        switchButton1.setSwitch(mSwitch);
    }
}
