package uz.pdp.lesson37pdp1.views;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import uz.pdp.lesson37pdp1.R;

/**
 * Created by Sherzodbek on 16.02.2018.
 */

public class SwitchButton extends android.support.v7.widget.AppCompatImageButton {
    private boolean mSwitch;
    private OnSwitchListener onSwitchListener;

    public SwitchButton(Context context) {
        super(context);
        init();
    }

    public SwitchButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SwitchButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setImageResource(R.drawable.switch_off);
        setBackgroundColor(Color.TRANSPARENT);
        setScaleType(ScaleType.FIT_XY);
        setOnClickListener(view -> setSwitch(!isSwitch()));
    }

    public void setOnSwitchListener(OnSwitchListener onSwitchListener) {
        this.onSwitchListener = onSwitchListener;
    }

    public boolean hasListener() {
        return onSwitchListener != null;
    }

    public void setSwitch(boolean mSwitch) {
        this.mSwitch = mSwitch;
        setImageResource(mSwitch ? R.drawable.switch_on : R.drawable.switch_off);
        if (hasListener()) onSwitchListener.onSwitch(this, mSwitch);
    }

    public boolean isSwitch() {
        return mSwitch;
    }

    public interface OnSwitchListener {
        void onSwitch(SwitchButton switchButton, boolean mSwitch);
    }
}
